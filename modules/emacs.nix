{ config, pkgs, inputs, ... }: {
  services.emacs.enable = true;
  services.emacs.package = pkgs.emacsPgtkNativeComp;
  home.packages = with pkgs; [
    ((emacsPackagesFor emacsPgtkNativeComp).emacsWithPackages (epkgs: [
      epkgs.vterm
      epkgs.pdf-tools
    ]))
    mu
    binutils
    zstd
    emacs-all-the-icons-fonts
    imagemagick
    rnix-lsp
    gnutls
    isync
    ispell    
  ];  
}
